= Apache Arrow Flight – ビッグデータ用高速データ転送フレームワーク

ビッグデータは現実的な時間で処理できてこそ活きます。処理時間はデータ処理アルゴリズムの計算量だけで決まり…ません！適切なノードにデータを配置しないとそもそもデータ処理できませんが、大量ノードが協調するビッグデータ処理ではデータ移動コストを無視できません。Apache Arrow Flightを使えばネットワーク帯域限界まで高速にデータ転送できます。

このセッションではApache Arrow Flightの仕組みを説明します。また、分散計算プラットフォームApache Arrow BallistaがApache Arrow Flightをどのように利用しているかも紹介します。

== ライセンス

=== スライド

CC BY-SA 4.0

原著作者：須藤功平

==== 株式会社クリアコードのロゴ

CC BY-SA 4.0

原著作者：株式会社クリアコード

ページヘッダーで使っています。

==== Apache Arrow Flightの図

Apache License 2.0

原著作者：The Apache Software Foundation

==== Apache Arrow Ballistaのロゴ

Apache License 2.0

原著作者：The Apache Software Foundation

== 作者向け

=== 表示

  rake

=== 公開

  rake publish

== 閲覧者向け

=== インストール

  gem install rabbit-slide-kou-db-tech-showcase-2021

=== 表示

  rabbit rabbit-slide-kou-db-tech-showcase-2021.gem

